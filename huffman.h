#ifndef HUFFMAN
#define HUFFMAN
#include <QFile>
#include <QString>
#include <QDebug>
#include <QTextStream>
#include <string.h>

void Write(QString Filename){
    QFile mFile(Filename);

    if(!mFile.open(QFile::WriteOnly | QFile::Text )){

        qDebug() << " não pode abrir o arquivo para escrever nele";
    return;
    }


    QTextStream out(&mFile);
    out << "aaabbcdddd";
    mFile.flush();
    mFile.close();

}

void Read(QString Filename){
    QFile mFile(Filename);

    if(!mFile.open(QFile::ReadOnly | QFile::Text )){

        qDebug() << " não pode abrir o arquivo para ler ";
    return;
    }



    QTextStream in(&mFile);
    QString mText = in.readAll();

    qDebug() << mText;
    mFile.close();

}





#endif // HUFFMAN


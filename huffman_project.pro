#-------------------------------------------------
#
# Project created by QtCreator 2015-05-11T08:15:34
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = huffman_project
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    huffman.h \
    countbytes.h
